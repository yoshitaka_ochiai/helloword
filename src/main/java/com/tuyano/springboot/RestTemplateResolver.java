package com.tuyano.springboot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class RestTemplateResolver {

	/**
	 * ここのメソッド名とserviceクラスで定義した@Qualifierアノテーションの内容が一致していないといけない
	 * 
	 * @return
	 */
	@Bean
	public RestTemplate zipCodeSearchRestTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		MappingJackson2HttpMessageConverter messageConverter =
			new MappingJackson2HttpMessageConverter();
		List<MediaType> supportedMediaTypes =
			new ArrayList<>(messageConverter.getSupportedMediaTypes());
		supportedMediaTypes.add(MediaType.TEXT_PLAIN);
		// text/plainのJacksonの処理対象にくわえる
		messageConverter.setSupportedMediaTypes(supportedMediaTypes);
		restTemplate.setMessageConverters(Collections.singletonList(messageConverter));
		// カスタムしたHttpMessageConverterを適用

		return restTemplate;
	}

	/**
	 * 上のメソッドでも下のメソッドでも同じことをしています
	 * 
	 * @return
	 */
	@Bean
	public RestTemplate restaurantSearchRestTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		List<HttpMessageConverter<?>> converters =
			restTemplate.getMessageConverters();
		for (HttpMessageConverter<?> converter : converters) {
			if (converter instanceof MappingJackson2HttpMessageConverter) {
				MappingJackson2HttpMessageConverter jsonConverter =
					(MappingJackson2HttpMessageConverter) converter;
				jsonConverter.setObjectMapper(new ObjectMapper());
//      jsonConverter.setSupportedMediaTypes(ImmutableList.of(new MediaType(
//			"application", "json", MappingJackson2HttpMessageConverter.DEFAULT_CHARSET),
//			new MediaType("text", "javascript", MappingJackson2HttpMessageConverter.DEFAULT_CHARSET)));
				List<MediaType> list = new ArrayList<MediaType>();
				list.add(MediaType.TEXT_XML);
				jsonConverter.setSupportedMediaTypes(list);
			}
		}
		return restTemplate;
	}

}