package com.tuyano.springboot;

public class RestaurantDetailDto {

	String id;

	String name;

	RestaurantBudgetDetailDto genre;

	RestaurantBudgetDetailDto budget;

	String access;

	String address;

	String open;

	String close;

	RestaurantBudgetDetailDto photo;

	/**
	 * @return id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id セットする id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name セットする name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return genre
	 */
	public RestaurantBudgetDetailDto getGenre() {
		return genre;
	}

	/**
	 * @param genre セットする genre
	 */
	public void setGenre(RestaurantBudgetDetailDto genre) {
		this.genre = genre;
	}

	/**
	 * @return budget
	 */
	public RestaurantBudgetDetailDto getBudget() {
		return budget;
	}

	/**
	 * @param budget セットする budget
	 */
	public void setBudget(RestaurantBudgetDetailDto budget) {
		this.budget = budget;
	}

	/**
	 * @return access
	 */
	public String getAccess() {
		return access;
	}

	/**
	 * @param access セットする access
	 */
	public void setAccess(String access) {
		this.access = access;
	}

	/**
	 * @return address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address セットする address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return open
	 */
	public String getOpen() {
		return open;
	}

	/**
	 * @param open セットするopen
	 */
	public void setOpen(String open) {
		this.open = open;
	}

	/**
	 * @return close
	 */
	public String getClose() {
		return close;
	}

	/**
	 * @param close セットする close
	 */
	public void setClose(String close) {
		this.close = close;
	}

	/**
	 * @return photo
	 */
	public RestaurantBudgetDetailDto getPhoto() {
		return photo;
	}

	/**
	 * @param photo セットする photo
	 */
	public void setPhoto(RestaurantBudgetDetailDto photo) {
		this.photo = photo;
	}
}