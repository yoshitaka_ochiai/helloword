package com.tuyano.springboot;

public class ResultDto {

	RestaurantDto results;

	/**
	 * @return results
	 */
	public RestaurantDto getResults() {
		return results;
	}

	/**
	 * @param results セットする results
	 */
	public void setResults(RestaurantDto results) {
		this.results = results;
	}

}
