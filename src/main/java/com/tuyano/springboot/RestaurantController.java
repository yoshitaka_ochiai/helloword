package com.tuyano.springboot;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import com.tuyano.springboot.RestaurantDto;
import com.tuyano.springboot.RestaurantService;
import com.tuyano.springboot.repositories.Eat2Repository;

@Controller
class RestaurantController {

	@Autowired
	private RestaurantService restaurantService;

	@Autowired
	Eat2Repository repository;

	@RequestMapping(value = "/r_top.html", method = RequestMethod.POST)
	public ModelAndView topform(HttpServletRequest request, ModelAndView mav) {
		mav.setViewName("r_top");

		mav.addObject("sid", request.getParameter("sid"));
		mav.addObject("name", request.getParameter("name"));
		mav.addObject("genre_name", request.getParameter("genre_name"));
		mav.addObject("budget_average", request.getParameter("budget_average"));
		mav.addObject("access", request.getParameter("access"));
		mav.addObject("address", request.getParameter("address"));
		mav.addObject("open", request.getParameter("open"));
		mav.addObject("close", request.getParameter("close"));

		Double starAvg = repository.findAllOrderByShop(request.getParameter("sid"));
		String comment = repository.getComment(request.getParameter("sid"));

		mav.addObject("savg", starAvg);
		mav.addObject("scom", comment);

		return mav;
	}

	@RequestMapping(value = "/r_Map.html", method = RequestMethod.POST)
	public ModelAndView mapform(HttpServletRequest request, ModelAndView mav) {
		mav.setViewName("r_Map");

		mav.addObject("sid", request.getParameter("sid"));
		mav.addObject("name", request.getParameter("name"));
		mav.addObject("genre_name", request.getParameter("genre_name"));
		mav.addObject("budget_average", request.getParameter("budget_average"));
		mav.addObject("access", request.getParameter("access"));
		mav.addObject("address", request.getParameter("address"));
		mav.addObject("open", request.getParameter("open"));
		mav.addObject("close", request.getParameter("close"));

		return mav;
	}

	@RequestMapping(value = "/user_review.html", method = RequestMethod.POST)
	public ModelAndView reviewform(HttpServletRequest request, ModelAndView mav) {
		mav.setViewName("user_review");

		mav.addObject("sid", request.getParameter("sid"));
		mav.addObject("name", request.getParameter("name"));
		mav.addObject("genre_name", request.getParameter("genre_name"));
		mav.addObject("budget_average", request.getParameter("budget_average"));
		mav.addObject("access", request.getParameter("access"));
		mav.addObject("address", request.getParameter("address"));
		mav.addObject("open", request.getParameter("open"));
		mav.addObject("close", request.getParameter("close"));

		return mav;
	}
	
	

	@RequestMapping(value = "/user_review", method = RequestMethod.POST)
	@Transactional(readOnly = false)
	public ModelAndView form(@ModelAttribute("formModel") Eat2 eat, ModelAndView mav) {
		repository.saveAndFlush(eat);
		return new ModelAndView("redirect:/out_review.html");
	}
	
	@RequestMapping("/rest.html")
	public ModelAndView restaurantForm(HttpSession session, ModelAndView mav) {
		final String uri = "http://webservice.recruit.co.jp/hotpepper/gourmet/v1/?key=5e720bab2a1eab28&address=高円寺&format=json";

		RestTemplate restTemplate = new RestTemplate();
		String result = restTemplate.getForObject(uri, String.class);

		// APIサービス呼び出し
		RestaurantDto restaurantDto = restaurantService.service("高円寺");
		// thymeleafでリストを展開して表示する

		mav.addObject("datalist", restaurantDto);
		mav.setViewName("db");

		return mav;
	}
}