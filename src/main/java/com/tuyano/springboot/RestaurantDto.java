package com.tuyano.springboot;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

public class RestaurantDto {

	String api_version;

	int results_available;

	String results_returned;

	int results_start;

	/** 飲食店詳細情報リスト */
	@JacksonXmlElementWrapper(useWrapping = false)
	// ここがポイント！受け取りデータのrootにリストがある場合
	// アノテーション付与(https://qiita.com/kagamihoge/items/c0f9bbb0f46a962b310e)
	List<RestaurantDetailDto> shop = new ArrayList<>();

	/**
	 * @return api_version
	 */
	public String getApi_version() {
		return api_version;
	}

	/**
	 * @param api_version セットする api_version
	 */
	public void setApi_version(String api_version) {
		this.api_version = api_version;
	}

	/**
	 * @return results_available
	 */
	public int getResults_available() {
		return results_available;
	}

	/**
	 * @param results_available セットする results_available
	 */
	public void setResults_available(int results_available) {
		this.results_available = results_available;
	}

	/**
	 * @return results_returned
	 */
	public String getResults_returned() {
		return results_returned;
	}

	/**
	 * @param results_returned セットする results_returned
	 */
	public void setResults_returned(String results_returned) {
		this.results_returned = results_returned;
	}

	/**
	 * @return results_start
	 */
	public int getResults_start() {
		return results_start;
	}

	/**
	 * @param results_start セットする results_start
	 */
	public void setResults_start(int results_start) {
		this.results_start = results_start;
	}

	/**
	 * @return shop
	 */
	public List<RestaurantDetailDto> getShop() {
		return shop;
	}

	/**
	 * @param shop セットする shop
	 */
	public void setShop(List<RestaurantDetailDto> shop) {
		this.shop = shop;
	}

}
