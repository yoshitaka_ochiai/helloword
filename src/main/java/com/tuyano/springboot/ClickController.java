package com.tuyano.springboot;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ClickController {

	@RequestMapping(value = "/index.html", method = RequestMethod.GET)
	public ModelAndView index(ModelAndView mav) {
		mav.setViewName("index");
		return mav;
	}

	@RequestMapping(value = "/r_top.html", method = RequestMethod.GET)
	public ModelAndView r_top(ModelAndView mav) {
		mav.setViewName("r_top");
		return mav;
	}
	
	@RequestMapping(value = "/r_evaluation.html", method = RequestMethod.GET)
	public ModelAndView r_evaluation(ModelAndView mav) {
		mav.setViewName("r_evaluation");
		return mav;
	}
	
	@RequestMapping(value = "/r_Map.html", method = RequestMethod.GET)
	public ModelAndView r_Map(ModelAndView mav) {
		mav.setViewName("r_Map");
		return mav;
	}
	
	@RequestMapping(value = "/user_review.html", method = RequestMethod.GET)
	public ModelAndView user_review(ModelAndView mav) {
		mav.setViewName("user_review");
		return mav;
	}
	
	@RequestMapping(value = "/out_review.html", method = RequestMethod.GET)
	public ModelAndView out_review(ModelAndView mav) {
		mav.setViewName("out_review");
		return mav;
	}
}