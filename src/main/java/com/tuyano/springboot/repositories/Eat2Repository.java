package com.tuyano.springboot.repositories;

import com.tuyano.springboot.Eat2;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface Eat2Repository extends JpaRepository<Eat2, Long> {
	
	@Query(value =" select case when avg(star) is NULL then 0.0 else truncate(avg(star),2) end as star from eat2 where sid= :sid",nativeQuery = true)
	double findAllOrderByShop(@Param("sid") String sid);
	
	@Query(value =" select case when hyouka is NULL then 'コメントはまだありません' else hyouka end as hyouka from eat2 where sid = :sid ORDER BY hyouka desc LIMIT 1;",nativeQuery = true)
	String getComment(@Param("sid") String sid);
	
}
