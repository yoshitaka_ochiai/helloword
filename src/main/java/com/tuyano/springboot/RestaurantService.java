package com.tuyano.springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.tuyano.springboot.RestaurantDto;

@Service
public class RestaurantService {

	@Autowired
	@Qualifier("restaurantSearchRestTemplate")
	RestTemplate restTemplate;

	// 本来はJSON形式で受け取りたかったがなぜか取得データのMediaTypeが
	// javascriptと判断され、処理できないためXML形式で受信する
	/** API リクエストURL */
	private static final String URL =
		"http://webservice.recruit.co.jp/hotpepper/gourmet/v1/?key=5e720bab2a1eab28&address={address}";

	public RestaurantDto service(String address) {
		return restTemplate.getForObject(URL, RestaurantDto.class, address);
	}

}