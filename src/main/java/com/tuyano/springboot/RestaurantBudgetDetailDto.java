package com.tuyano.springboot;

public class RestaurantBudgetDetailDto {

	String average;

	String name;

	RestaurantPhotoDetailDto pc;

	/**
	 * @return average
	 */
	public String getAverage() {
		return average;
	}

	/**
	 * @param average セットする average
	 */
	public void setAverage(String average) {
		this.average = average;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name セットする name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return pc
	 */
	public RestaurantPhotoDetailDto getPc() {
		return pc;
	}

	/**
	 * @param pc セットする pc
	 */
	public void setPc(RestaurantPhotoDetailDto pc) {
		this.pc = pc;
	}
}
